# Prezi
## ... eine dynamische Alternative zu Powerpoint

### Was ist Prezi?
Prezi ist ein Präsentationsprogramm, das seit 2009 auf dem Markt ist und eine echte Konkurrenz für das klassische Präsentationstool PowerPoint darstellt. Im Gegensatz zu Microsoft PowerPoint ist Prezi eine Webanwendung. Für die Nutzung muss ein Online Konto eingerichtet und ein Tarif ausgewählt werden: Es gibt einen kostenlosen Basic-Tarif, bei dem alle Inhalte öffentlich sind. Weitere kostenpflichtige Tarife müssen monatlich oder jährlich gezahlt werden und ermöglichen es den Nutzern weitere Tools zu verwenden und Prezi Offline zu nutzen. Die Anwendung steht sowohl für den Mac als auch für Windows zur Verfügung.
Ein Hauptmerkmal von Prezi ist, dass die Inhalte, Texte, Grafiken und andere Darstellungen auf einer großen Fläche nebeneinander angeordnet sind. Diese Objekte werden auf einem Pfad in eine beliebige Reihenfolge gebracht und können jederzeit im „großen Ganzen“ betrachtet werden. Bei der Präsentation werden die Objekte auf dieser Fläche der festgelegten Pfadreihenfolge entsprechend angefahren und angezoomt.

### Wann sollte man Powerpoint vorziehen?
Bevor man sich für die Präsentationsform Prezi entscheidet, sollte man den Kontext berücksichtigen und technische Fragen klären. 
- 	Wird die Präsentation von Ihnen präsentiert oder von den Zuschauern selbst abgerufen und gesteuert`
- 	Erfolgt die Präsentation auf einer großen Leinwand oder einem kleinen Bildschirm?
- 	Besteht eine Internetverbindung während der Präsentation?
- 	Wie viel Zeit zur Vorbereitung haben Sie eingeplant?

Prezi wird nicht bei Fachvorträgen empfohlen, da kurzfristige Änderungen mit Prezi schwieriger umzusetzen sind, als bei PowerPoint. Zudem lenken die Zoom-Effekte und Kamerafahrten von Ihnen, dem Vortragenden, ab.
Wenn Sie Ihren Vortrag mit einem iPad oder Tablet steuern, können Sie die Funktionen von Prezi noch besser nutzen, denn dass heranzoomen und schenken funktioniert mit dem Touchscreen noch besser. Wenn also eine kreative, überzeugende Präsentation geplant ist, die die Zuhörer begeistert, kann Prezi durchaus erfolgsversprechend sein.
Wird den Zuhörern/Zuschauern die Möglichkeit gegeben, sich die Präsentation freiwillig und selbstgesteuert anzuschauen, eignet sich ebenfalls Prezi hervorragend, um die Aufmerksamkeit des Zuschauers zu gewinnen. Daher können Prezi-Präsentationen als sinnvolle Ergänzung zu den üblichen Informationen einer Website dienen.

#### Beispiele 
Was ist Prezi?
https://www.youtube.com/watch?v=3CMLESMdxDI

Tutorials:
-	https://www.youtube.com/watch?v=fjE6RRagtCY
-	https://www.youtube.com/watch?v=NGjM49BXFEg

#### Quellen
- Elferink, Daniel (2014). Prezi Handbuch. Version 2.7. XinXii Verlag.
- https://praesentare.com/prezi-powerpoint
